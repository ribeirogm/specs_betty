package tutorial;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.util.BambooServer;

@BambooSpec
public class PlanSpec {

    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("http://bamboo:8085");

        final Plan plan = CreatePlan.plan("PlanA", "PLANA");
        final Plan plan1 = CreatePlan.plan("PlanB", "PLANB");
        final Plan plan2 = CreatePlan.plan("PlanC", "PLANC");

        final PlanPermissions perm = CreatePermission.planPermission("PROJ","PLANA");
        final PlanPermissions perm1 = CreatePermission.planPermission("PROJ","PLANB");
        final PlanPermissions perm2 = CreatePermission.planPermission("PROJ","PLANC");
        // Publish plans
        bambooServer.publish(plan);
        bambooServer.publish(plan1);
        bambooServer.publish(plan2);
        // Publish Plan and permissions for x86_64-30kEcoli
        bambooServer.publish(perm);
        bambooServer.publish(perm1);
        bambooServer.publish(perm2);

    }
}