package tutorial;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;

public class CreatePermission {
    public static PlanPermissions planPermission(String projkey, String plankey) {
        final PlanPermissions planPermission = new PlanPermissions(new PlanIdentifier(projkey, plankey))
                .permissions(new Permissions()
                        .userPermissions("root", PermissionType.EDIT, PermissionType.VIEW, PermissionType.ADMIN, PermissionType.CLONE, PermissionType.BUILD));
        return planPermission;
    }
}
