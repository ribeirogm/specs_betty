package tutorial;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CreatePlan {

    public static Plan plan(String name, String plankey) {

        try (InputStream input = CreatePlan.class.getClassLoader().getResourceAsStream("config.properties")) {

            Properties prop = new Properties();

            if (input == null) {
                System.out.println("Sorry, unable to find config.properties");
                return null;
            }

            //load a properties file from class path, inside static method
            prop.load(input);
            final Plan plan = new Plan(new Project()
                    .key(new BambooKey("PROJ"))
                    .name("Project")
                    .description(name), name,
                    new BambooKey(plankey))
                    .pluginConfigurations(new ConcurrentBuilds())
                    .stages(new Stage("Default Stage")
                            .jobs(new Job("Default Job",
                                    new BambooKey("JOB1"))
                                    .tasks(new ScriptTask()
                                            .description("Echo variables")
                                            .inlineBody("la -la"))))
                    .variables(new Variable("genome",
                                    prop.getProperty(name + ".genome")),
                            new Variable("library",
                                    prop.getProperty(name + ".library")),
                            new Variable("tracefile",
                                    prop.getProperty(name + ".tracefile")))
                    .planBranchManagement(new PlanBranchManagement()
                            .delete(new BranchCleanup())
                            .notificationForCommitters());
            return plan;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }


}
